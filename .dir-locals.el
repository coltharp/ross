;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((rust-ts-mode . ((eglot-workspace-configuration . (:rust-analyzer
                                                    (:check
                                                     (:allTargets :json-false :command "clippy")
                                                     :cargo
                                                     (:target "x86_64-ross.json")
                                                     :imports
                                                     (:preferNoStd t)))))))
