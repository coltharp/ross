#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use ross::{gdt, test::Test};
use spin::Once;
use x86_64::structures::idt::InterruptDescriptorTable;

#[test_case]
fn test_should_panic() {
    gdt::init();

    static IDT: Once<InterruptDescriptorTable> = Once::new();
    IDT.call_once(|| {
        let mut idt = InterruptDescriptorTable::new();
        let entry_options = idt
            .double_fault
            .set_handler_fn(ross::test::should_fault_handler_diverging_error);
        unsafe {
            entry_options.set_stack_index(gdt::IstIndex::DoubleFault as u16);
        }
        idt
    })
    .load();

    #[allow(unconditional_recursion)]
    fn stack_overflow() {
        stack_overflow();
        // Prevent TCO.
        let x = 0;
        let ptr = &x as *const i32;
        unsafe {
            ptr.read_volatile();
        }
    }
    stack_overflow();
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();
    loop {}
}

pub fn test_runner(tests: &[&dyn Test]) {
    ross::test::should_panic_runner(tests);
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    ross::test::panic_handler(info)
}
