#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(ross::test::runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use alloc::{boxed::Box, vec::Vec};
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;

// Basic heap allocation
#[test_case]
fn simple_allocation() {
    let heap_value_1 = Box::new(41);
    let heap_value_2 = Box::new(13);
    assert_eq!(*heap_value_1, 41);
    assert_eq!(*heap_value_2, 13);
}

// Incrementally allocate a large Vec.
#[test_case]
fn large_vec() {
    let n = 1000;
    let mut vec = Vec::new();
    for i in 0..n {
        vec.push(i);
    }
    assert_eq!(vec.iter().sum::<u64>(), (n - 1) * n / 2);
}

// Allocate and deallocate a number of boxes that, if they were allocated all at
// once, ought to exhaust the heap.
#[test_case]
fn many_boxes() {
    for i in 0..ross::heap::SIZE {
        let x = Box::new(i);
        assert_eq!(*x, i);
    }
}

#[test_case]
fn many_boxes_long_lived() {
    let long_lived = Box::new(1); // new
    for i in 0..ross::heap::SIZE {
        let x = Box::new(i);
        assert_eq!(*x, i);
    }
    assert_eq!(*long_lived, 1); // new
}

entry_point!(main);

fn main(boot_info: &'static BootInfo) -> ! {
    unsafe {
        ross::init(boot_info);
    }
    test_main();
    ross::hlt_loop();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    ross::test::panic_handler(info)
}
