#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(ross::test::runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use ross::{vga, vga_println};

#[test_case]
fn test_vga_println() {
    unsafe {
        vga::init();
    }
    vga_println!("test_vga_println output");
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();
    loop {}
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    ross::test::panic_handler(info);
}
