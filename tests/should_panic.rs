#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use ross::test::Test;

#[test_case]
fn test_should_panic() {
    panic!();
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();
    loop {}
}

pub fn test_runner(tests: &[&dyn Test]) {
    ross::test::should_panic_runner(tests);
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    ross::test::should_panic_handler(info);
}
