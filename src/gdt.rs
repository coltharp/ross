//! Global descriptor table.  Useful mainly for loading a TSS.
use core::cell::SyncUnsafeCell;
use spin::Once;
use x86_64::{
    instructions::segmentation::{Segment, CS},
    structures::{
        gdt::{Descriptor, GlobalDescriptorTable, SegmentSelector},
        tss::TaskStateSegment,
    },
    VirtAddr,
};

pub enum IstIndex {
    DoubleFault = 0,
}

/// Initializes the global descriptor table (GDT).
pub fn init() {
    static TSS: Once<TaskStateSegment> = Once::new();
    static GDT: Once<(GlobalDescriptorTable, SegmentSelector, SegmentSelector)> = Once::new();
    let (gdt, code_selector, tss_selector) = GDT.call_once(|| {
        let tss = TSS.call_once(|| {
            let mut tss = TaskStateSegment::new();
            tss.interrupt_stack_table[IstIndex::DoubleFault as usize] = {
                const STACK_SIZE: usize = 0x1000;
                static STACK: SyncUnsafeCell<[u8; STACK_SIZE]> =
                    SyncUnsafeCell::new([0; STACK_SIZE]);
                let stack_start = VirtAddr::from_ptr(STACK.get());
                stack_start + STACK_SIZE
            };
            tss
        });

        let mut gdt = GlobalDescriptorTable::new();
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(tss));
        (gdt, code_selector, tss_selector)
    });
    gdt.load();
    unsafe {
        // Set the code segment register.
        CS::set_reg(*code_selector);
        // Load the TSS.
        x86_64::instructions::tables::load_tss(*tss_selector);
    }
}
