//! Various examples and obsolete things.

use core::{
    alloc::{GlobalAlloc, Layout},
    ptr,
    task::{RawWaker, RawWakerVTable, Waker},
};
use x86_64::{
    structures::paging::{
        FrameAllocator, Mapper, OffsetPageTable, Page, PageTableFlags, PhysFrame, Size4KiB,
    },
    PhysAddr,
};

/// Creates an example mapping for the given page to frame `0xb8000`.
///
/// # Safety
///
/// This function is safe only if you never use the frame `0xb8000` anywhere
/// else.  In particular, this function is always unsafe if used in conjunction
/// with the `vga` module.
pub unsafe fn create_example_mapping(
    page: Page,
    mapper: &mut OffsetPageTable,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) {
    let frame = PhysFrame::containing_address(PhysAddr::new(0xb8000));
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
    unsafe { mapper.map_to(page, frame, flags, frame_allocator) }
        .expect("map_to failed")
        .flush();
}

/// A `FrameAllocator` that never allocates any pages.
pub struct EmptyFrameAllocator;

unsafe impl FrameAllocator<Size4KiB> for EmptyFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        None
    }
}

/// A heap allocator that always fails.
pub struct DummyAlloc;

unsafe impl GlobalAlloc for DummyAlloc {
    unsafe fn alloc(&self, _layout: Layout) -> *mut u8 {
        ptr::null_mut()
    }

    unsafe fn dealloc(&self, _ptr: *mut u8, _layout: Layout) {
        panic!("DummyAlloc::dealloc");
    }
}

/// Creates a `RawWaker` that does nothing.
pub fn dummy_raw_waker() -> RawWaker {
    fn no_op(_: *const ()) {}
    fn clone(_: *const ()) -> RawWaker {
        dummy_raw_waker()
    }
    let vtable = &RawWakerVTable::new(clone, no_op, no_op, no_op);
    RawWaker::new(ptr::null(), vtable)
}

/// Creates a `Waker` that does nothing.
pub fn dummy_waker() -> Waker {
    unsafe { Waker::from_raw(dummy_raw_waker()) }
}
