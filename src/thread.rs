use core::arch::global_asm;

use crossbeam_queue::ArrayQueue;
use spin::{Mutex, Once};
use x86_64::{
    registers::{
        rflags::RFlags,
        segmentation::{Segment, CS, SS},
    },
    structures::idt::InterruptStackFrame,
    VirtAddr,
};

use crate::{
    idt::{self, InterruptIndex},
    pic, vm,
};

fn threads() -> &'static Mutex<ArrayQueue<VirtAddr>> {
    const MAX_THREADS: usize = 128;
    // The queue of threads.  Right now, a thread is just a pointer to a stack.
    // Threads are scheduled in a round-robin fashion.  Right now, the
    // scheduling algorithm is tightly coupled to the rest of the
    // implementation, but this can be easily changed.
    static THREADS: Once<Mutex<ArrayQueue<VirtAddr>>> = Once::new();
    THREADS.call_once(|| Mutex::new(ArrayQueue::new(MAX_THREADS)))
}

/// Creates and starts a new thread.
///
/// Note that the thread function is required to return `!`, which is to say
/// that it must never return.  This is a temporary restriction that frees us
/// from having to worry about cleaning up threads that have finished execution.
pub fn spawn(f: fn() -> !) {
    // Each thread needs its own stack.  This static variable stores the base
    // pointer to the next thread's stack.
    static PAGE: Once<Mutex<VirtAddr>> = Once::new();
    // Right now, a stack is just one 4KiB page.
    const STACK_SIZE: usize = 0x1000;

    let page_base = {
        // Lock the virtual memory allocator.
        let mut vm = vm::get().lock();
        // Get the base pointer.
        let mut page_base = PAGE
            .call_once(|| Mutex::new(VirtAddr::try_new(0x4444_8888_0000).unwrap()))
            .lock();
        let addr = *page_base;
        // Map the stack pages.
        vm.map_addr_range(addr, STACK_SIZE).unwrap();
        // Increment the base pointer.
        *page_base += STACK_SIZE;
        addr
    };
    // Set the stack pointer to the top of the stack, one past the last byte.
    let rsp = (page_base + STACK_SIZE).as_mut_ptr::<u64>();
    // When the thread first runs, it will be returning from a timer interrupt,
    // so it will expect to see 20 words on the stack: 15 registers and a 5-word
    // interrupt stack frame.  The registers don't matter, but the stack frame
    // needs to have certain values.  We write those values here.
    unsafe {
        // Stack segment.
        rsp.offset(-1).write(SS::get_reg().0 as u64);
        // Stack pointer.  Note that we will later decrement the stack pointer
        // by 20, but that is a conceptually different stack pointer.  *This*
        // pointer points to the (conceptual) top of the stack *after* the
        // return-from-interrupt, which should just be the highest address of
        // the stack.  *That* pointer points to the location of the stack
        // *before* popping registers returning from the interrupt.
        rsp.offset(-2).write(rsp.addr() as u64);
        // CPU flags.  We enable interrupts, because otherwise the thread would
        // never be preempted.
        rsp.offset(-3).write(RFlags::INTERRUPT_FLAG.bits());
        // Code segment.
        rsp.offset(-4).write(CS::get_reg().0 as u64);
        // Instruction pointer.
        rsp.offset(-5).write((f as *const ()).addr() as u64);
    }
    // To avoid potential UB, set all the registers to 0.  (Unclear if it would
    // be UB to not do so, but might as well err on the side of caution.)
    for i in -20..-5 {
        unsafe {
            rsp.offset(i).write(0);
        }
    }
    // Now decrement the stack pointer by 20.
    let rsp = unsafe { rsp.offset(-20) };
    // Finally, enqueue the thread for execution.
    let threads = threads().lock();
    threads
        .push(VirtAddr::try_new(rsp.addr() as u64).unwrap())
        .unwrap();
}

extern "x86-interrupt" {
    /// Timer interrupt.
    fn timer(stack_frame: InterruptStackFrame);
}
global_asm!(r#"
.global timer
timer:
    # Right now, the stack pointer is pointing to the frame for the current
    # interrupt.  When we call `iretq`, the CPU will pop it off the stack and
    # use its fields, including the PC, to resume execution.  The "trick" is to
    # just keep the frame where it is and make the stack pointer point to the
    # stack of a thread that was previously switched out.  That stack will
    # contain a frame from the last switch-out, so calling `iretq` will then
    # resume the execution of *that* thread.

    # Disable interrupts.  Unclear whether this is strictly necessary, but it
    # can't hurt.
    cli

    # Push all general-purpose registers.  Note that our build disables MMX,
    # SSE, and the FPU, so this really is everything.
    push r15
    push r14
    push r13
    push r12
    push r11
    push r10
    push r9
    push r8
    push rdi
    push rsi
    push rbp
    push rbx
    push rdx
    push rcx
    push rax

    # Pass the stack pointer as an argument to `switch`.  This is the context of
    # the current thread.  (In the SysV x86-64 ABI, `rdi` is the first argument
    # register.)
    mov rdi, rsp
    # Call `switch`, which enqueues the context of the thread that we are
    # switching out and returns the context of the thread to be switched in.
    # (This is interpolated# see the end of the `global_asm` call.)
    call {}
    # Set the stack pointer to the value returned by `switch`.  We are now in
    # the context of the new thread.  (In the SysV x86-64 ABI, `rax` contains
    # the return value.)
    mov rsp, rax

    # Pop the registers of the new thread off the stack.  They were pushed there
    # when the thread was last switched out.
    pop rax
    pop rcx
    pop rdx
    pop rbx
    pop rbp
    pop rsi
    pop rdi
    pop r8
    pop r9
    pop r10
    pop r11
    pop r12
    pop r13
    pop r14
    pop r15

    # Now the stack pointer is pointing to the interrupt stack frame from the
    # last switch-out.  When we call `iretq`, it will be popped, restoring the
    # PC.
    iretq
"#, sym switch
);

/// Given the context of a currently-executing thread, enqueue that context and
/// return the context of the next thread to be executed.
///
/// This function uses the SysV x86-64 ABI.  The reason for this is that we call
/// it from assembly, so we need it to conform to *some* well-defined ABI, but
/// Rust does not have a well-defined ABI.  We don't need to use `sysv64`
/// specifically, but we do need to use something.
extern "sysv64" fn switch(rsp: VirtAddr) -> VirtAddr {
    let threads = threads().lock();
    threads.push(rsp).unwrap();
    let rsp = threads.pop().unwrap();
    let mut pic = pic::get().lock();
    unsafe {
        pic.notify_end_of_interrupt(InterruptIndex::Timer as u8);
    }
    rsp
}

/// Initialize the threading subsystem.  Currently, this just installs the timer
/// interrupt handler.
pub fn init() {
    idt::update(|idt| {
        idt[InterruptIndex::Timer as usize].set_handler_fn(unsafe {
            // The timer interrupt handler is written in assembly, so it is
            // `unsafe`, but `set_handler_fn` does not allow the handler
            // function to be `unsafe`.  We get around this by transmuting the
            // timer handler to a safe function.
            core::mem::transmute(timer as unsafe extern "x86-interrupt" fn(InterruptStackFrame))
        });
    })
}
