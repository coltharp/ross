//! Virtual memory subsystem.

use bootloader::{
    bootinfo::{MemoryMap, MemoryRegionType},
    BootInfo,
};
use spin::{Mutex, Once};
use x86_64::{
    registers::control::Cr3,
    structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, OffsetPageTable, Page, PageTable,
        PageTableFlags, PhysFrame, Size4KiB,
    },
    PhysAddr, VirtAddr,
};

pub struct Vm {
    pub frame_allocator: BootInfoFrameAllocator,
    pub mapper: OffsetPageTable<'static>,
}

#[derive(Debug)]
pub enum MapError {
    FrameAllocation,
    MapTo(MapToError<Size4KiB>),
}

impl Vm {
    pub fn map_addr(&mut self, addr: VirtAddr) -> Result<(), MapError> {
        self.map_page(Page::containing_address(addr))
    }

    pub fn map_addr_range(&mut self, start: VirtAddr, size: usize) -> Result<(), MapError> {
        let start_page = Page::containing_address(start);
        let end_page = Page::containing_address(start + size - 1_usize);
        for page in Page::range_inclusive(start_page, end_page) {
            self.map_page(page)?;
        }
        Ok(())
    }

    pub fn map_page(&mut self, page: Page) -> Result<(), MapError> {
        let frame = self
            .frame_allocator
            .allocate_frame()
            .ok_or(MapError::FrameAllocation)?;
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
        unsafe {
            self.mapper
                .map_to(page, frame, flags, &mut self.frame_allocator)
        }
        .map_err(MapError::MapTo)?
        .flush();
        Ok(())
    }
}

static VM: Once<Mutex<Vm>> = Once::new();

pub fn get() -> &'static Mutex<Vm> {
    VM.get().expect("vm::get: not initialized")
}

/// Initializes the virtual memory system.
///
/// # Safety
///
/// The caller must guarantee that the provided `BootInfo` is accurate.
pub unsafe fn init(boot_info: &'static BootInfo) {
    VM.call_once(|| {
        Mutex::new({
            let frame_allocator = unsafe { BootInfoFrameAllocator::new(&boot_info.memory_map) };
            let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
            let mapper = unsafe {
                let level_4_table = active_level_4_table(phys_mem_offset);
                OffsetPageTable::new(level_4_table, phys_mem_offset)
            };
            Vm {
                frame_allocator,
                mapper,
            }
        })
    });
}

/// Returns a mutable reference to the active level 4 page table.
///
/// # Safety
///
/// - The caller must guarantee that the complete physical memory is mapped to
///   virtual memory at `physical_memory_offset`.
/// - This function must be only called once.
unsafe fn active_level_4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable {
    let (level_4_table_frame, _) = Cr3::read();
    let phys = level_4_table_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_table_ptr: *mut PageTable = virt.as_mut_ptr();
    unsafe { &mut *page_table_ptr }
}

type BootInfoFrameIterator = impl Iterator<Item = PhysFrame>;

/// A `FrameAllocator` that returns usable frames from the bootloader's memory
/// map.
pub struct BootInfoFrameAllocator {
    inner: BootInfoFrameIterator,
}

impl BootInfoFrameAllocator {
    /// Creates a `BootInfoFrameAllocator` from the passed memory map.
    ///
    /// # Safety
    ///
    /// The caller must guarantee that the passed memory map is valid. The main
    /// requirement is that all frames that are marked as `USABLE` in it are
    /// really unused.
    pub unsafe fn new(memory_map: &'static MemoryMap) -> Self {
        BootInfoFrameAllocator {
            inner: memory_map
                .iter()
                // Filter out unusable regions.
                .filter(|r| r.region_type == MemoryRegionType::Usable)
                // Iterate over each memory region in increments of 4 KiB.
                .flat_map(|r| (r.range.start_addr()..r.range.end_addr()).step_by(4096))
                // For each 4 KiB increment, yield a frame object.
                .map(|addr| PhysFrame::containing_address(PhysAddr::new(addr))),
        }
    }
}

unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        self.inner.next()
    }
}
