use core::{
    pin::Pin,
    task::{Context, Poll},
};

use conquer_once::spin::OnceCell;
use crossbeam_queue::ArrayQueue;
use futures_util::{task::AtomicWaker, Stream, StreamExt};
use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
use x86_64::structures::idt::InterruptStackFrame;

use crate::{
    idt::{self, InterruptIndex},
    pic, vga, vga_print, vga_println,
};

type ScancodeQueue = ArrayQueue<u8>;

const QUEUE_SIZE: usize = 128;
static QUEUE: OnceCell<ArrayQueue<u8>> = OnceCell::uninit();

fn queue() -> &'static ScancodeQueue {
    QUEUE.get().expect("keyboard::queue: not initialized")
}

static WAKER: AtomicWaker = AtomicWaker::new();

pub fn push_scancode(scancode: u8) {
    let queue = queue();
    if queue.push(scancode).is_err() {
        vga_println!("WARNING: dropping keyboard input");
    } else {
        WAKER.wake();
    }
}

pub struct ScancodeStream {
    _private: (),
}

#[allow(clippy::new_without_default)]
impl ScancodeStream {
    pub fn new() -> ScancodeStream {
        QUEUE
            .try_init_once(|| ArrayQueue::new(QUEUE_SIZE))
            .expect("ScancodeStream::new: called twice");
        ScancodeStream { _private: () }
    }
}

impl Stream for ScancodeStream {
    type Item = u8;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let queue = queue();
        WAKER.register(cx.waker());
        match queue.pop() {
            Some(scancode) => {
                WAKER.take();
                Poll::Ready(Some(scancode))
            }
            None => Poll::Pending,
        }
    }
}

/// Reads incoming keypresses and echoes them to the VGA buffer.
pub async fn vga_echo_keypresses() {
    let mut scancodes = ScancodeStream::new();
    let mut keyboard = Keyboard::new(
        ScancodeSet1::new(),
        layouts::Us104Key,
        HandleControl::Ignore,
    );

    while let Some(scancode) = scancodes.next().await {
        if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
            if let Some(key) = keyboard.process_keyevent(key_event) {
                match key {
                    DecodedKey::Unicode('\x08') => {
                        vga::writer().lock().backspace();
                    }
                    DecodedKey::Unicode(character) => vga_print!("{}", character),
                    _ => (),
                }
            }
        }
    }
}

pub fn init() {
    idt::update(|idt| {
        idt[InterruptIndex::Keyboard as usize].set_handler_fn(keyboard);
    });
}

extern "x86-interrupt" fn keyboard(_stack_frame: InterruptStackFrame) {
    use x86_64::instructions::port::Port;

    let mut port = Port::new(0x60);
    let scancode: u8 = unsafe { port.read() };
    push_scancode(scancode);

    let mut pic = pic::get().lock();
    unsafe {
        pic.notify_end_of_interrupt(InterruptIndex::Keyboard as u8);
    }
}
