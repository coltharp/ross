pub mod qemu;

use crate::{serial_print, serial_println};
use core::panic::PanicInfo;
use x86_64::structures::idt::InterruptStackFrame;

/// Trait for objects that can be run as tests.
pub trait Test {
    fn run(&self);
}

impl<F> Test for F
where
    F: Fn(),
{
    fn run(&self) {
        serial_print!("{}\t", core::any::type_name::<F>());
        self();
        serial_println!("OK");
    }
}

/// Test runner.
pub fn runner(tests: &[&dyn Test]) -> ! {
    serial_println!("Running {} tests", tests.len());
    for test in tests {
        test.run();
    }
    qemu::exit(qemu::Status::Success);
}

/// Runner for tests that should panic.
pub fn should_panic_runner(tests: &[&dyn Test]) -> ! {
    match tests {
        [] => qemu::exit(qemu::Status::Success),
        [test] => {
            test.run();
            serial_println!("ERR");
            serial_println!("(Was supposed to panic, but didn't)");
            qemu::exit(qemu::Status::Failure);
        }
        _ => {
            serial_println!("Can't run more than 1 should_panic test");
            serial_println!("(Tried to run {} tests)", tests.len());
            qemu::exit(qemu::Status::Failure);
        }
    }
}

/// Panic handler for testing.
pub fn panic_handler(info: &PanicInfo) -> ! {
    serial_println!("ERR");
    serial_println!("{}", info);
    qemu::exit(qemu::Status::Failure);
}

/// Panic handler for tests that should panic.
pub fn should_panic_handler(_info: &PanicInfo) -> ! {
    serial_println!("OK");
    qemu::exit(qemu::Status::Success);
}

/// `DivergingHandlerFuncWithErrCode` for tests in which such faults should
/// occur.
pub extern "x86-interrupt" fn should_fault_handler_diverging_error(
    _stack_frame: InterruptStackFrame,
    _error_code: u64,
) -> ! {
    serial_println!("OK");
    qemu::exit(qemu::Status::Success);
}
