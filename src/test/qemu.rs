/// QEMU exit statuses.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
#[repr(u32)]
pub enum Status {
    Success = 0x10,
    Failure = 0x11,
}

/// Exit QEMU testing with the given status.
pub fn exit(status: Status) -> ! {
    use x86_64::instructions::port::Port;
    unsafe {
        let mut port = Port::new(0xf4);
        port.write(status as u32);
    }
    crate::hlt_loop();
}
