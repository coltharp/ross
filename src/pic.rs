//! Programmable interrupt controller.

use pic8259::ChainedPics;
use spin::{Mutex, Once};

/// Offset of the PIC in the interrupt vector.
pub const OFFSET: u8 = 32;
const OFFSET_2: u8 = OFFSET + 8;

/// Initialize the PIC.
pub fn init() {
    let mut pic = get().lock();
    unsafe {
        pic.initialize();
    }
}

pub fn get() -> &'static Mutex<ChainedPics> {
    pub static PIC: Once<Mutex<ChainedPics>> = Once::new();
    PIC.call_once(|| {
        let pic = unsafe { ChainedPics::new(OFFSET, OFFSET_2) };
        Mutex::new(pic)
    })
}
