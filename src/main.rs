#![no_std]
#![no_main]
// Lints
#![allow(clippy::empty_loop)]
#![deny(unsafe_op_in_unsafe_fn)]
// Testing
#![feature(custom_test_frameworks)]
#![test_runner(ross::test::runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use ross::{
    keyboard,
    task::{Executor, Task},
    thread, vga_println,
};

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    #[cfg(test)]
    test_main();

    unsafe { ross::init(boot_info) };
    vga_println!("Welcome to ross! ☺");

    thread::spawn(|| {
        let mut x = 0;
        loop {
            vga_println!("Thread 1: {x}");
            x += 1;
            for _ in 0..2000000 {}
        }
    });
    thread::spawn(|| {
        let mut x = 0;
        loop {
            vga_println!("Thread 2: {x}");
            x -= 1;
            for _ in 0..2000000 {}
        }
    });

    thread::init();

    let mut executor = Executor::new();
    executor
        .spawn(Task::new(keyboard::vga_echo_keypresses()))
        .unwrap();
    executor.run();
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    vga_println!("{}", info);
    ross::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    ross::test::panic_handler(info);
}
