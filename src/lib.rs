#![no_std]
// Enable the x86 interrupt calling convention, which is necessary for writing
// interrupt handlers.
#![feature(abi_x86_interrupt)]
// Allow certain operations in const contexts.
#![feature(const_nonnull_new)]
#![feature(const_option)]
// Allow inline const expressions.  Used in the block allocator.
#![feature(inline_const)]
// Things for MaybeUninit arrays.  Used in the threading module.
#![feature(maybe_uninit_array_assume_init)]
#![feature(maybe_uninit_uninit_array)]
// Allow certain convenience functions for NonNull.
#![feature(non_null_convenience)]
// I have no idea what I'm doing.
#![feature(strict_provenance)]
#![feature(exposed_provenance)]
#![warn(fuzzy_provenance_casts)]
#![warn(lossy_provenance_casts)]
// `SyncUnsafeCell` is essentially a replacement for `mut static`.  We use this
// in `gdt.rs`.
#![feature(sync_unsafe_cell)]
#![feature(try_blocks)]
// Allow `type Foo = impl Bar` aliases, which lets us use `impl Bar` values as
// struct fields.  Used in in `vm.rs`.
#![feature(type_alias_impl_trait)]
// Require unsafe operations to be delimited within an unsafe block, even in
// unsafe functions.
#![deny(unsafe_op_in_unsafe_fn)]
// Testing.
#![cfg_attr(test, no_main)]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test::runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

pub mod example;
pub mod gdt;
pub mod heap;
pub mod idt;
pub mod keyboard;
pub mod pic;
pub mod serial;
pub mod task;
pub mod test;
pub mod thread;
pub mod vga;
pub mod vm;

use bootloader::BootInfo;

#[cfg(test)]
bootloader::entry_point!(kernel_main);

#[cfg(test)]
#[doc(hidden)]
pub fn kernel_main(boot_info: &'static BootInfo) -> ! {
    unsafe {
        init(boot_info);
    }
    test_main();
    hlt_loop();
}

/// Initialize the operating system.
///
/// In particular, this sets up
///
/// - Virtual memory
/// - The heap
/// - Interrupts
/// - Keyboard input
///
/// # Safety
///
/// In general, this function is safe only if the x86 hardware is accessed only
/// through `ross` APIs.
pub unsafe fn init(boot_info: &'static BootInfo) {
    unsafe {
        vga::init();
    }
    unsafe { vm::init(boot_info) };
    heap::init().expect("Heap initialization failed");
    gdt::init();
    idt::init();
    pic::init();
    keyboard::init();
    x86_64::instructions::interrupts::enable();
}

/// Loop forever, always executing the `hlt` instruction.
pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}

#[cfg(test)]
#[doc(hidden)]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    test::panic_handler(info);
}
