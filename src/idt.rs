//! Interrupts.

use crate::{gdt, pic, vga_println};
use spin::{Mutex, Once};
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = pic::OFFSET,
    Keyboard,
}

fn get() -> &'static Mutex<InterruptDescriptorTable> {
    static IDT: Once<Mutex<InterruptDescriptorTable>> = Once::new();
    IDT.call_once(|| Mutex::new(InterruptDescriptorTable::new()))
}

pub fn update<F>(f: F)
where
    F: FnOnce(&mut InterruptDescriptorTable),
{
    let mut idt = get().lock();
    f(&mut idt);
    unsafe { idt.load_unsafe() };
}

/// Install interrupt handlers.
pub fn init() {
    update(|idt| {
        idt.breakpoint.set_handler_fn(breakpoint);
        let entry_options = idt.double_fault.set_handler_fn(double_fault);
        unsafe {
            entry_options.set_stack_index(gdt::IstIndex::DoubleFault as u16);
        }
        idt.page_fault.set_handler_fn(page_fault);
        idt[InterruptIndex::Timer as usize].set_handler_fn(timer);
    });
}

extern "x86-interrupt" fn breakpoint(frame: InterruptStackFrame) {
    vga_println!("EXCEPTION: BREAKPOINT\n{:#?}", frame);
}

#[test_case]
fn test_breakpoint_exception() {
    x86_64::instructions::interrupts::int3();
}

extern "x86-interrupt" fn double_fault(frame: InterruptStackFrame, error_code: u64) -> ! {
    panic!(
        "EXCEPTION: DOUBLE FAULT\nError code: {:#x}\n{:#?}",
        error_code, frame
    );
}

extern "x86-interrupt" fn page_fault(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    use x86_64::registers::control::Cr2;

    vga_println!("EXCEPTION: PAGE FAULT");
    vga_println!("Accessed address: {:?}", Cr2::read());
    vga_println!("Error code: {:?}", error_code);
    vga_println!("{:#?}", stack_frame);
    crate::hlt_loop();
}

extern "x86-interrupt" fn timer(_stack_frame: InterruptStackFrame) {
    let mut pic = pic::get().lock();
    unsafe {
        pic.notify_end_of_interrupt(InterruptIndex::Timer as u8);
    }
}
