mod simple_executor;
use crossbeam_queue::ArrayQueue;
pub use simple_executor::SimpleExecutor;

use core::{
    future::Future,
    pin::Pin,
    sync::atomic::{AtomicU64, Ordering},
    task::{Context, Poll, Waker},
};

use alloc::{boxed::Box, collections::BTreeMap, sync::Arc, task::Wake};

/// A task ID.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
struct TaskId(u64);

impl TaskId {
    /// Generates a new task ID, which is automatically distinct from all others.
    fn new() -> TaskId {
        static NEXT_ID: AtomicU64 = AtomicU64::new(0);
        TaskId(NEXT_ID.fetch_add(1, Ordering::Relaxed))
    }
}

/// An asynchronous kernel task.
pub struct Task {
    id: TaskId,
    future: Pin<Box<dyn Future<Output = ()>>>,
}

impl Task {
    /// Creates a new task.
    pub fn new<T>(future: T) -> Task
    where
        T: Future<Output = ()> + 'static,
    {
        Task {
            id: TaskId::new(),
            future: Box::pin(future),
        }
    }

    /// Polls the task for its completion status.  This has the same overall
    /// semantics as `Future::poll`.
    fn poll(&mut self, cx: &mut Context) -> Poll<()> {
        self.future.as_mut().poll(cx)
    }
}

const MAX_TASKS: usize = 128;

struct TaskWaker {
    id: TaskId,
    queue: Arc<ArrayQueue<TaskId>>,
}

impl TaskWaker {
    fn waker(id: TaskId, queue: Arc<ArrayQueue<TaskId>>) -> Waker {
        Waker::from(Arc::new(TaskWaker { id, queue }))
    }

    fn wake_task(&self) {
        self.queue
            .push(self.id)
            .expect("task::TaskWaker::wake: queue full (this should be impossible!)");
    }
}

impl Wake for TaskWaker {
    fn wake(self: Arc<TaskWaker>) {
        self.wake_task();
    }

    fn wake_by_ref(self: &Arc<TaskWaker>) {
        self.wake_task();
    }
}

/// An executor for kernel tasks.
pub struct Executor {
    tasks: BTreeMap<TaskId, Task>,
    queue: Arc<ArrayQueue<TaskId>>,
    wakers: BTreeMap<TaskId, Waker>,
    /// The number of currently-active tasks.  This is used to ensure that there
    /// are never more active tasks than the queue can hold, so that a `Waker`
    /// will never fail to add its task to the queue.
    num_tasks: usize,
}

impl Default for Executor {
    fn default() -> Self {
        Self::new()
    }
}

impl Executor {
    /// Creates a new executor.
    pub fn new() -> Executor {
        Executor {
            tasks: BTreeMap::new(),
            queue: Arc::new(ArrayQueue::new(MAX_TASKS)),
            wakers: BTreeMap::new(),
            num_tasks: 0,
        }
    }

    /// Spawns a new task.  Returns `None` if the task queue is full.
    #[must_use]
    pub fn spawn(&mut self, task: Task) -> Option<()> {
        if self.num_tasks == MAX_TASKS {
            return None;
        }
        self.queue.push(task.id).ok()?;
        if self.tasks.insert(task.id, task).is_some() {
            panic!("task::Executor::spawn: task submitted twice (this should be impossible!)");
        }
        self.num_tasks += 1;
        Some(())
    }

    /// Run the executor.  Make sure you've submitted at least one task before
    /// you call this, as it never returns!
    pub fn run(&mut self) -> ! {
        loop {
            while let Some(id) = self.queue.pop() {
                let task = match self.tasks.get_mut(&id) {
                    None => continue,
                    Some(task) => task,
                };
                let waker = self
                    .wakers
                    .entry(id)
                    .or_insert_with(|| TaskWaker::waker(id, self.queue.clone()));
                let mut cx = Context::from_waker(waker);
                if let Poll::Ready(()) = task.poll(&mut cx) {
                    self.tasks.remove(&id);
                    self.wakers.remove(&id);
                    self.num_tasks -= 1;
                }
            }

            x86_64::instructions::interrupts::disable();
            if self.queue.is_empty() {
                x86_64::instructions::interrupts::enable_and_hlt();
            } else {
                x86_64::instructions::interrupts::enable();
            }
        }
    }
}
