use core::task::{Context, Poll};

use super::Task;
use alloc::collections::VecDeque;

#[derive(Default)]
pub struct SimpleExecutor {
    tasks: VecDeque<Task>,
}

/// A very simple task executor.  Not for real-world use, as it eats as much CPU
/// power as it possibly can.
impl SimpleExecutor {
    /// Constructs a new `SimpleExecutor`.
    pub fn new() -> SimpleExecutor {
        SimpleExecutor {
            tasks: VecDeque::new(),
        }
    }

    /// Spawns a new task on this executor.
    pub fn spawn(&mut self, task: Task) {
        self.tasks.push_back(task)
    }

    /// Runs this executor until all tasks complete.
    pub fn run(&mut self) {
        // The simplest possible thing: just repeatedly loop through the task
        // queue, removing tasks that are done, until there is no task left.
        while let Some(mut task) = self.tasks.pop_front() {
            let waker = crate::example::dummy_waker();
            let mut cx = Context::from_waker(&waker);
            match task.poll(&mut cx) {
                Poll::Ready(()) => {}
                Poll::Pending => self.tasks.push_back(task),
            }
        }
    }
}
