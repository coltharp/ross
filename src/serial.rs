//! Printing via the serial interface.

use spin::{Mutex, Once};
use uart_16550::SerialPort;

/// Returns the first serial port behind a mutex.
pub fn serial1() -> &'static Mutex<SerialPort> {
    static SERIAL1: Once<Mutex<SerialPort>> = Once::new();
    SERIAL1.call_once(|| {
        let mut serial_port = unsafe { SerialPort::new(0x3F8) };
        serial_port.init();
        Mutex::new(serial_port)
    })
}

/// Prints text to the serial interface.
#[macro_export]
macro_rules! serial_print {
    ($($arg:tt)*) => {
        $crate::serial::_print(format_args!($($arg)*));
    };
}

/// Prints a line of text to the serial interface.
#[macro_export]
macro_rules! serial_println {
    () => ($crate::serial_print!("\n"));
    ($fmt:expr) => ($crate::serial_print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => ($crate::serial_print!(
        concat!($fmt, "\n"), $($arg)*));
}

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    use core::fmt::Write;
    x86_64::instructions::interrupts::without_interrupts(|| serial1().lock().write_fmt(args))
        .unwrap_or_else(|err| panic!("Printing to serial interface: {}", err))
}
