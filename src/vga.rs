//! Print text via the VGA text buffer.

use core::{
    fmt::{self, Write},
    ptr,
};
use spin::{Mutex, Once};

/// VGA text colors.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

/// A set of VGA text attributes.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
#[repr(transparent)]
pub struct Attributes(u8);

impl Attributes {
    /// Creates a new set of attributes.
    pub const fn new(
        foreground: Color,
        bright: bool,
        background: Color,
        blink: bool,
    ) -> Attributes {
        Attributes(
            (blink as u8) << 7 | (background as u8) << 4 | (bright as u8) << 3 | foreground as u8,
        )
    }
}

impl Default for Attributes {
    fn default() -> Self {
        Attributes::new(Color::LightGray, false, Color::Black, false)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash, Default)]
#[repr(C)]
struct Character {
    code_point: u8,
    attributes: Attributes,
}

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

/// VGA text writer.
pub struct Writer {
    row: usize,
    col: usize,
    buffer: ptr::NonNull<Character>,
    pub attributes: Attributes,
}

// Safety: the reason Writer does not implement Send by default is because it
// contains a pointer.  However, the `new` function for `Writer` requires a
// promise that the `Writer` gets exclusive access to the underlying memory.
// Furthermore, there is no way to duplicate the pointer contained by a
// `Writer`.  Therefore, it is safe to `Send` a writer across threads.
unsafe impl Send for Writer {}

impl Writer {
    /// Create a new `Writer`.
    ///
    /// # Safety
    ///
    /// The provided pointer must be valid, and the `Writer` must have exclusive
    /// access to the underlying memory.
    unsafe fn new(buffer: ptr::NonNull<Character>) -> Self {
        let mut writer = Writer {
            row: 0,
            col: 0,
            buffer,
            attributes: Attributes::default(),
        };
        // Initialize VGA memory.
        writer.clear();
        writer
    }

    /// Given an index into the VGA buffer, return a pointer to the
    /// corresponding memory location.
    fn index(&self, row: usize, col: usize) -> ptr::NonNull<Character> {
        assert!(
            row < BUFFER_HEIGHT && col < BUFFER_WIDTH,
            "vga::index: out of bounds"
        );
        // Safety: the preceding assertions, plus the safety contract of `new`,
        // ensure that the resulting pointer will point into valid memory.
        unsafe { self.buffer.add(row * BUFFER_WIDTH + col) }
    }

    /// Write a VGA character at the given position.
    fn write(&self, row: usize, col: usize, character: Character) {
        let ptr = self.index(row, col);
        // SAFETY The pointer returned by `index` is guaranteed to point into
        // memory.
        unsafe {
            ptr.write_volatile(character);
        }
    }

    /// Read a VGA character at the given position.
    fn read(&self, row: usize, col: usize) -> Character {
        let ptr = self.index(row, col);
        // SAFETY The pointer returned by `index` is guaranteed to be valid.
        // This function can only be called after `new`, which initializes the
        // underlying memory.
        unsafe { ptr.read_volatile() }
    }

    /// Writes a single code page 437 byte.
    pub fn write_u8(&mut self, byte: u8) {
        if self.col + 1 == BUFFER_WIDTH {
            self.newline();
        }
        self.write(
            self.row,
            self.col,
            Character {
                code_point: byte,
                attributes: self.attributes,
            },
        );
        self.col += 1;
    }

    /// Writes a single Unicode character, converting to code page 437 if
    /// necessary.  Skips characters that cannot be converted.
    pub fn write_char(&mut self, c: char) {
        let b = match c {
            '\x00' => 0x00,
            '☺' => 0x01,
            '☻' => 0x02,
            '♥' => 0x03,
            '♦' => 0x04,
            '♣' => 0x05,
            '♠' => 0x06,
            '•' => 0x07,
            '◘' => 0x08,
            '○' => 0x09,
            '◙' => 0x0A,
            '♂' => 0x0B,
            '♀' => 0x0C,
            '♪' => 0x0D,
            '♫' => 0x0E,
            '☼' => 0x0F,
            '►' => 0x10,
            '◄' => 0x11,
            '↕' => 0x12,
            '‼' => 0x13,
            '¶' => 0x14,
            '§' => 0x15,
            '▬' => 0x16,
            '↨' => 0x17,
            '↑' => 0x18,
            '↓' => 0x19,
            '→' => 0x1A,
            '←' => 0x1B,
            '∟' => 0x1C,
            '↔' => 0x1D,
            '▲' => 0x1E,
            '▼' => 0x1F,
            ' '..='~' => c as u8,
            '⌂' => 0x7F,
            'Ç' => 0x80,
            'ü' => 0x81,
            'é' => 0x82,
            'â' => 0x83,
            'ä' => 0x84,
            'à' => 0x85,
            'å' => 0x86,
            'ç' => 0x87,
            'ê' => 0x88,
            'ë' => 0x89,
            'è' => 0x8A,
            'ï' => 0x8B,
            'î' => 0x8C,
            'ì' => 0x8D,
            'Ä' => 0x8E,
            'Å' => 0x8F,
            'É' => 0x90,
            'æ' => 0x91,
            'Æ' => 0x92,
            'ô' => 0x93,
            'ö' => 0x94,
            'ò' => 0x95,
            'û' => 0x96,
            'ù' => 0x97,
            'ÿ' => 0x98,
            'Ö' => 0x99,
            'Ü' => 0x9A,
            '¢' => 0x9B,
            '£' => 0x9C,
            '¥' => 0x9D,
            '₧' => 0x9E,
            'ƒ' => 0x9F,
            'á' => 0xA0,
            'í' => 0xA1,
            'ó' => 0xA2,
            'ú' => 0xA3,
            'ñ' => 0xA4,
            'Ñ' => 0xA5,
            'ª' => 0xA6,
            'º' => 0xA7,
            '¿' => 0xA8,
            '⌐' => 0xA9,
            '¬' => 0xAA,
            '½' => 0xAB,
            '¼' => 0xAC,
            '¡' => 0xAD,
            '«' => 0xAE,
            '»' => 0xAF,
            '░' => 0xB0,
            '▒' => 0xB1,
            '▓' => 0xB2,
            '│' => 0xB3,
            '┤' => 0xB4,
            '╡' => 0xB5,
            '╢' => 0xB6,
            '╖' => 0xB7,
            '╕' => 0xB8,
            '╣' => 0xB9,
            '║' => 0xBA,
            '╗' => 0xBB,
            '╝' => 0xBC,
            '╜' => 0xBD,
            '╛' => 0xBE,
            '┐' => 0xBF,
            '└' => 0xC0,
            '┴' => 0xC1,
            '┬' => 0xC2,
            '├' => 0xC3,
            '─' => 0xC4,
            '┼' => 0xC5,
            '╞' => 0xC6,
            '╟' => 0xC7,
            '╚' => 0xC8,
            '╔' => 0xC9,
            '╩' => 0xCA,
            '╦' => 0xCB,
            '╠' => 0xCC,
            '═' => 0xCD,
            '╬' => 0xCE,
            '╧' => 0xCF,
            '╨' => 0xD0,
            '╤' => 0xD1,
            '╥' => 0xD2,
            '╙' => 0xD3,
            '╘' => 0xD4,
            '╒' => 0xD5,
            '╓' => 0xD6,
            '╫' => 0xD7,
            '╪' => 0xD8,
            '┘' => 0xD9,
            '┌' => 0xDA,
            '█' => 0xDB,
            '▄' => 0xDC,
            '▌' => 0xDD,
            '▐' => 0xDE,
            '▀' => 0xDF,
            'α' => 0xE0,
            'ß' => 0xE1,
            'Γ' => 0xE2,
            'π' => 0xE3,
            'Σ' => 0xE4,
            'σ' => 0xE5,
            'µ' => 0xE6,
            'τ' => 0xE7,
            'Φ' => 0xE8,
            'Θ' => 0xE9,
            'Ω' => 0xEA,
            'δ' => 0xEB,
            '∞' => 0xEC,
            'φ' => 0xED,
            'ε' => 0xEE,
            '∩' => 0xEF,
            '≡' => 0xF0,
            '±' => 0xF1,
            '≥' => 0xF2,
            '≤' => 0xF3,
            '⌠' => 0xF4,
            '⌡' => 0xF5,
            '÷' => 0xF6,
            '≈' => 0xF7,
            '°' => 0xF8,
            '∙' => 0xF9,
            '·' => 0xFA,
            '√' => 0xFB,
            'ⁿ' => 0xFC,
            '²' => 0xFD,
            '■' => 0xFE,
            ' ' => 0xFF,
            '\n' => {
                self.newline();
                return;
            }
            _ => 0x00,
        };
        self.write_u8(b);
    }

    /// Writes a string of Unicode characters, converting to code page 437 if
    /// necessary.  Skips characters that cannot be converted.
    pub fn write_str(&mut self, s: &str) {
        for c in s.chars() {
            self.write_char(c);
        }
    }

    /// Starts a new line.
    pub fn newline(&mut self) {
        // Set the cursor to the leftmost column.
        self.col = 0;

        // If we have not yet filled the screen, just set the cursor to the next
        // row.
        if self.row + 1 < BUFFER_HEIGHT {
            self.row += 1;
            return;
        }

        // At this point, we know that we are on the last row, so we need to
        // "scroll".  The first step is to copy the existing lines upward.
        for i in 0..BUFFER_HEIGHT - 1 {
            for j in 0..BUFFER_WIDTH {
                let c = self.read(i + 1, j);
                self.write(i, j, c);
            }
        }

        // Now blank the last line.
        for j in 0..BUFFER_WIDTH {
            self.write(BUFFER_HEIGHT - 1, j, Character::default());
        }
    }

    /// Performs a backspace action.
    pub fn backspace(&mut self) {
        if self.row == 0 && self.col == 0 {
            // Nothing to do.
            return;
        }
        let prev_row = if self.col == 0 {
            self.row - 1
        } else {
            self.row
        };
        let prev_col = (self.col + BUFFER_WIDTH - 1) % BUFFER_WIDTH;
        self.write(prev_row, prev_col, Character::default());
        self.row = prev_row;
        self.col = prev_col;
    }

    /// Clear the VGA buffer and reset the cursor position.
    pub fn clear(&mut self) {
        self.row = 0;
        self.col = 0;
        for i in 0..BUFFER_HEIGHT {
            for j in 0..BUFFER_WIDTH {
                self.write(i, j, Character::default());
            }
        }
    }
}

impl Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_str(s);
        Ok(())
    }
}

static WRITER: Once<Mutex<Writer>> = Once::new();

/// Returns the single `Writer` instance behind a mutex.  Panics if the writer
/// has not been initialized.
pub fn writer() -> &'static Mutex<Writer> {
    WRITER.get().expect("vga::writer: not initialized")
}

/// Initialize the VGA text writer.
///
/// # Safety
///
/// This function is safe only if the `vga` module has exclusive access to the
/// VGA memory.
pub unsafe fn init() {
    WRITER.call_once(|| {
        // It is OK to call from_exposed_addr_mut here, because the VGA memory
        // is outside of the Rust abstract machine: no Rust object lives there.
        let buffer = ptr::NonNull::new(ptr::from_exposed_addr_mut(0xb8000)).unwrap();
        let writer = unsafe { Writer::new(buffer) };
        Mutex::new(writer)
    });
}

// TODO Compile-time validation via a proc macro?

/// Prints text to the VGA buffer.
#[macro_export]
macro_rules! vga_print {
    ($($arg:tt)*) => ($crate::vga::_print(format_args!($($arg)*)));
}

/// Prints a line of text to the VGA buffer.
#[macro_export]
macro_rules! vga_println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::vga_print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    x86_64::instructions::interrupts::without_interrupts(|| {
        writer().lock().write_fmt(args).unwrap()
    });
}

#[test_case]
fn test_println_simple() {
    vga_println!("test_println_simple output");
}

#[test_case]
fn test_println_many() {
    for _ in 0..200 {
        vga_println!("test_println_many output");
    }
}

#[test_case]
fn test_println_output() {
    let s = "Some test string that fits on a single line";
    let mut writer = writer().lock();
    writer.clear();
    writeln!(writer, "\n{}", s).expect("writeln failed");
    for (i, c) in s.chars().enumerate() {
        let screen_char = writer.read(1, i);
        assert_eq!(char::from(screen_char.code_point), c);
    }
}
