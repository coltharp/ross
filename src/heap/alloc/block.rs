//! Block allocator.

use core::{alloc::Layout, ptr};

use super::GlobalAllocMut;

/// A block allocator.
pub struct Block<'a> {
    /// The list heads for each block class.
    heads: [Option<&'a mut Header<'a>>; BLOCK_SIZES.len()],
    /// The fallback allocator, used in case a block class has no free blocks or a
    /// requested allocation is larger than any block class.
    fallback: linked_list_allocator::Heap,
}

/// A block header, which occurs at the beginning of a block.
struct Header<'a> {
    /// Reference to the next block in the list of blocks that have the same
    /// size as this block.
    next: Option<&'a mut Header<'a>>,
}

/// Block class sizes.  Note that we ensure that a block's alignment is always
/// equal to its size.
const BLOCK_SIZES: [usize; 9] = [8, 16, 32, 64, 128, 256, 512, 1024, 2048];

/// Finds the index into [`BLOCK_SIZES`] of the smallest block class that will
/// accommodate the given layout.
fn block_classify(layout: Layout) -> Option<usize> {
    // Since each block has alignment equal to its size, we need to make sure
    // that we find a block whose size is greater than both the size and the
    // alignment of the given layout.  Conversely, once we have done that, we
    // don't need to do any further work to ensure correct alignment.
    let size = layout.size().max(layout.align());
    BLOCK_SIZES.iter().position(|&s| s >= size)
}

impl<'a> Block<'a> {
    /// Tries to allocate memory via the fallback allocator.
    fn fallback_alloc(&mut self, layout: Layout) -> *mut u8 {
        match self.fallback.allocate_first_fit(layout) {
            Ok(ptr) => ptr.as_ptr(),
            Err(_) => ptr::null_mut(),
        }
    }
}

unsafe impl GlobalAllocMut for Block<'static> {
    unsafe fn new(start: ptr::NonNull<u8>, size: usize) -> Self {
        let mut fallback = linked_list_allocator::Heap::empty();
        // SAFETY This is why we require a 'static lifetime: the linked list
        // allocator demands ownership for the entire lifetime of the program.
        // A more flexible fallback allocator could in turn let us be more
        // flexible.
        unsafe {
            fallback.init(start.as_ptr(), size);
        }
        Block {
            heads: [const { None }; BLOCK_SIZES.len()],
            fallback,
        }
    }

    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let index = match block_classify(layout) {
            None => return self.fallback_alloc(layout),
            Some(index) => index,
        };
        // Remove the front node of the given block class.
        match self.heads[index].take() {
            Some(header) => {
                // Replace with the next node.
                self.heads[index] = header.next.take();
                header as *mut Header as *mut u8
            }
            None => {
                // No free blocks of the correct class; allocate a new one.
                let block_size = BLOCK_SIZES[index];
                // Block sizes and layouts are equal.
                let layout = Layout::from_size_align(block_size, block_size).unwrap();
                self.fallback_alloc(layout)
            }
        }
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        match block_classify(layout) {
            None => {
                let pointer = ptr::NonNull::new(ptr).unwrap();
                // SAFETY We have deduced that this memory was allocated via the
                // fallback allocator.
                unsafe {
                    self.fallback.deallocate(pointer, layout);
                }
            }
            Some(index) => {
                let header = Header {
                    next: self.heads[index].take(),
                };
                let ptr = ptr as *mut Header;
                // SAFETY The pointer has been deallocated, so there is not an
                // object living there, and it fits within a block class, so it
                // must be aligned properly.
                unsafe {
                    ptr.write(header);
                }
                self.heads[index] = Some(
                    // SAFETY We have just written a `Header` there, and we will
                    // not alias.
                    unsafe { ptr.as_mut() }.unwrap(),
                )
            }
        }
    }
}
