//! Linked list allocator.

use core::{alloc::Layout, mem, num::NonZeroUsize, ptr};

use super::GlobalAllocMut;

/// A linked list-based allocator.
pub struct LinkedList<'a> {
    /// Base pointer of the heap.
    start: ptr::NonNull<u8>,
    /// The head node of the list.  This is always a dummy node with a size of
    /// 0.
    head: Header<'a>,
}

/// A node in the region list, which occurs as a header at the beginning of a
/// region.
struct Header<'a> {
    size: usize,
    next: Option<&'a mut Header<'a>>,
}

impl<'a> LinkedList<'a> {
    /// Adds a given region to the free list.
    ///
    /// # Safety
    ///
    /// - The region must be a subset of the heap.
    /// - The region must be valid to write a [`Header`] to.
    /// - The region must be disjoint from all active allocations.
    /// - The region must be disjoint from all other regions in the free list.
    unsafe fn add_free_region(&mut self, start: ptr::NonNull<u8>, size: usize) {
        assert!(
            super::is_aligned::<Header>(start.as_ptr().addr()),
            "LinkedList::add_free_region: region not aligned"
        );
        assert!(
            size >= mem::size_of::<Header>(),
            "LinkedList::add_free_region: region too small"
        );
        // Create a `Header` object describing the new region.  The `Header`
        // object itself is currently on the stack.
        let mut header = Header::new(size);
        // We are going to add the new region to just after the initial dummy
        // node, so move the dummy node's `next` into our `next`.
        header.next = self.head.next.take();
        // Write the `Header` to the start of the region.
        let mut header_ptr = start.cast::<Header>();
        // SAFETY This pointer is not null.  It is inside the heap, so it is
        // valid for writes.  It is aligned for `Header`s.  It is disjoint from
        // all active allocations and from the rest of the free list, so there
        // are no aliasing problems.
        unsafe {
            header_ptr.write(header);
        }
        // Make the `next` field of the dummy node point to the new header.
        self.head.next = Some(
            // SAFETY The pointer is aligned and initialized.  There are no
            // other pointers to the header, and there will not be as long as it
            // exists.
            unsafe { header_ptr.as_mut() },
        );
    }

    /// Tries to find a free region that will accommodate the given layout.  If
    /// one is found, returns the starting address of the region.  Also adjusts
    /// the list so that the returned region is disjoint from all free regions.
    fn find_region(&mut self, layout: Layout) -> Option<NonZeroUsize> {
        let mut prev = &mut self.head;
        while let Some(current) = &mut prev.next {
            if let Some(alloc_start) = current.alloc(layout) {
                if alloc_start == current.start_addr() {
                    // The free region includes the header itself, so remove it
                    // from the list.  (Unfortunately, we cant just return the
                    // address of the header, because that is not guaranteed to
                    // be aligned correctly.)
                    prev.next = current.next.take();
                } else {
                    // There is some space left.  (That space might be 0 bytes,
                    // though.)
                    current.size -= current.end_addr().get() - alloc_start.get();
                }
                return Some(alloc_start);
            }
            prev = prev.next.as_mut().unwrap();
        }
        None
    }
}

impl<'a> Header<'a> {
    /// Creates a new header with the given size and no `next`.
    fn new(size: usize) -> Header<'a> {
        Header { size, next: None }
    }

    /// Gets the address of the header itself.
    fn start_addr(&self) -> NonZeroUsize {
        (self as *const Header).addr().try_into().unwrap()
    }

    /// Gets the address at the end of the free region.
    fn end_addr(&self) -> NonZeroUsize {
        self.start_addr().checked_add(self.size).unwrap()
    }

    /// Tries to allocate memory with the given layout in the region described
    /// by this header.  Allocation proceeds from the end backward.
    fn alloc(&self, layout: Layout) -> Option<NonZeroUsize> {
        let alloc_end = self.end_addr().get();
        let alloc_start = alloc_end.checked_sub(layout.size())?;
        let alloc_start = super::align_down(alloc_start, layout.align());
        if alloc_start < self.start_addr().get() {
            None
        } else {
            Some(alloc_start.try_into().unwrap())
        }
    }
}

/// Adjusts a layout so that it can hold a [`Header`].  We need to do this when
/// allocating so that, when a region gets deallocated, we can add it back to
/// the list.
fn adjust_layout(layout: Layout) -> Option<Layout> {
    let layout = layout
        .align_to(mem::align_of::<Header>())
        .ok()?
        .pad_to_align();
    let size = layout.size().max(mem::size_of::<Header>());
    Layout::from_size_align(size, layout.align()).ok()
}

unsafe impl<'a> GlobalAllocMut for LinkedList<'a> {
    unsafe fn new(start: ptr::NonNull<u8>, size: usize) -> Self {
        let mut ret = LinkedList {
            start,
            head: Header::new(0),
        };
        // SAFETY Guaranteed by the contract of `new`.
        unsafe {
            ret.add_free_region(start, size);
        }
        ret
    }

    unsafe fn alloc(&mut self, layout: core::alloc::Layout) -> *mut u8 {
        let layout = match adjust_layout(layout) {
            None => return ptr::null_mut(),
            Some(layout) => layout,
        };
        let alloc_start = match self.find_region(layout) {
            None => return ptr::null_mut(),
            Some(alloc_start) => alloc_start,
        };
        self.start.with_addr(alloc_start).as_ptr()
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: core::alloc::Layout) {
        let layout = adjust_layout(layout).unwrap();
        let start = ptr::NonNull::new(ptr).expect("LinkedList::dealloc: null pointer");
        let size = layout.size();
        // SAFETY The region was originally obtained via `find_free_region`,
        // which guarantees that the returned region is disjoint from the rest
        // of the heap.
        unsafe {
            self.add_free_region(start, size);
        }
    }
}
