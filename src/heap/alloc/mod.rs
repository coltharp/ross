use core::{
    alloc::{GlobalAlloc, Layout},
    mem, ptr,
};
mod bump;
pub use bump::Bump;
mod linked_list;
pub use linked_list::LinkedList;
mod block;
pub use block::Block;
use spin::{Mutex, Once};

/// Computes the nearest address less than or equal to `addr` that has alignment
/// `align`.
fn align_down(addr: usize, align: usize) -> usize {
    let mask = align.checked_sub(1).expect("align_down: align = 0");
    addr & !mask
}

/// Indicates whether the given address is aligned for the given type.
fn is_aligned<T>(addr: usize) -> bool {
    align_down(addr, mem::align_of::<T>()) == addr
}

/// Like `GlobalAlloc`, but receives `&mut self`.
///
/// In more detail, this trait:
/// - Requires that the allocator provide a constructor.
/// - Gives the allocator an exclusive reference to itself in
///   [`alloc`](Self::alloc) and [`dealloc`](Self::dealloc).
///
/// # Safety
///
/// Implementors must satisfy all the safety requirements of `GlobalAlloc`.
unsafe trait GlobalAllocMut {
    /// Creates a new allocator.
    ///
    /// # Safety
    ///
    /// - There must be at least `size` bytes in the address space after
    ///   `start`.
    /// - The given region must be valid for reads and writes.
    /// - As long as the allocator exists, it must have exclusive access to the
    ///   given region.
    unsafe fn new(start: ptr::NonNull<u8>, size: usize) -> Self;

    /// Allocates memory.
    ///
    /// # Safety
    ///
    /// See the `alloc` method of `GlobalAlloc`.
    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8;

    /// Deallocates memory.
    ///
    /// # Safety
    ///
    /// See the `dealloc` method of `GlobalAlloc`.
    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout);
}

/// A `GlobalAlloc` that wraps an implementor of [`GlobalAllocMut`],
/// initializing it on demand and hiding it behind a mutex so that it can
/// receive exclusive references.
pub struct Locked<T> {
    /// Pointer to the start of the heap.
    start: ptr::NonNull<u8>,
    /// Size of the heap.
    size: usize,
    /// The inner allocator, protected by a mutex.
    inner: Mutex<Once<T>>,
}

// SAFETY `start` and `size` are never mutated, and `inner` is behind a mutex.
unsafe impl<T> Sync for Locked<T> {}

impl<T> Locked<T> {
    /// Creates a new `Locked` allocator.
    ///
    /// # Safety
    ///
    /// See the safety requirements of [`GlobalAllocMut`].
    pub const unsafe fn new(start: ptr::NonNull<u8>, size: usize) -> Locked<T> {
        Locked {
            start,
            size,
            inner: Mutex::new(Once::new()),
        }
    }

    /// Calls a function on the inner allocator, initializing it if necessary.
    fn with_inner<S, F>(&self, f: F) -> S
    where
        T: GlobalAllocMut,
        F: FnOnce(&mut T) -> S,
    {
        let mut inner = self.inner.lock();
        // SAFETY The caller of `Locked::new` has already guaranteed that
        // `self.start` and `self.size` describe a valid memory region.
        inner.call_once(|| unsafe { T::new(self.start, self.size) });
        f(inner.get_mut().unwrap())
    }
}

unsafe impl<T> GlobalAlloc for Locked<T>
where
    T: GlobalAllocMut,
{
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        // SAFETY We pass the safety obligation to `T::alloc`.
        self.with_inner(|inner| unsafe { inner.alloc(layout) })
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        // SAFETY We pass the safety obligation to `T::dealloc`.
        self.with_inner(|inner| unsafe { inner.dealloc(ptr, layout) })
    }
}
