//! Bump allocator.

use core::{alloc::Layout, num::NonZeroUsize, ptr};

use super::GlobalAllocMut;

/// A bump allocator.
pub struct Bump {
    /// Pointer to the first byte of the heap.
    start: ptr::NonNull<u8>,
    /// Address of the byte one past the end of the heap.
    end: NonZeroUsize,
    /// Address of the byte one past the next unallocated byte.
    cursor: NonZeroUsize,
    /// The number of active allocations.
    num_allocations: usize,
}

unsafe impl GlobalAllocMut for Bump {
    unsafe fn new(start: ptr::NonNull<u8>, size: usize) -> Self {
        let end = start
            .addr()
            .checked_add(size)
            .expect("Bump::new: invalid heap region");
        Bump {
            start,
            end,
            cursor: end,
            num_allocations: 0,
        }
    }

    // The heap grows downward.  See
    // https://fitzgeraldnick.com/2019/11/01/always-bump-downwards.html.
    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let start = match self.cursor.get().checked_sub(layout.size()) {
            None => return ptr::null_mut(),
            Some(start) => start,
        };
        let start = super::align_down(start, layout.align());
        if start < self.start.addr().get() {
            return ptr::null_mut();
        }
        self.cursor = start.try_into().unwrap();
        self.num_allocations += 1;
        self.start.as_ptr().with_addr(start)
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, _layout: Layout) {
        assert!(
            ptr >= self.start.as_ptr() && ptr.addr() < self.end.into(),
            "Bump::dealloc: not in heap"
        );
        assert!(
            ptr.addr() >= self.cursor.into(),
            "Bump::dealloc: not allocated"
        );
        self.num_allocations = self
            .num_allocations
            .checked_sub(1)
            .expect("Bump::dealloc: no active allocations");
        if self.num_allocations == 0 {
            self.cursor = self.end;
        }
    }
}
