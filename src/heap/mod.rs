//! Heap subsystem.

use core::ptr;

use x86_64::VirtAddr;

use crate::vm;

pub mod alloc;

/// Base address of the heap.
pub const START_ADDR: usize = 0x_4444_4444_0000;

/// Pointer to the beginning of the heap.
#[allow(fuzzy_provenance_casts)]
pub const START: ptr::NonNull<u8> = ptr::NonNull::new(START_ADDR as *mut u8).unwrap();

/// Size of the heap.
pub const SIZE: usize = 0x32 * 0x1000;

/// The global allocator.
#[global_allocator]
static ALLOCATOR: alloc::Locked<alloc::Block<'static>> = unsafe { alloc::Locked::new(START, SIZE) };

/// Initializes the heap.
pub fn init() -> Result<(), vm::MapError> {
    let mut vm = vm::get().lock();
    vm.map_addr_range(VirtAddr::new(START_ADDR as u64), SIZE)
}
